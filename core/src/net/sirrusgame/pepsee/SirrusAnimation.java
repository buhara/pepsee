package net.sirrusgame.pepsee;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SirrusAnimation {
	public float x = 100, y = 100;
	Animation walkAnimation;
	float frameDuration;
	TextureRegion[] walkFrames;
	SpriteBatch spriteBatch;
	TextureRegion currentFrame;
	boolean animated = true;
	ArrayList<AnimationSet> animationSets = new ArrayList<AnimationSet>();
	float stateTime;
	public Texture walkSheet;
	public int activeAnimationSetId;
	
	public static class AnimationParameters{
		public FileHandle fileHandle;
		public float frameDuration;
		public ArrayList<AnimationSet> animationSets = new ArrayList<AnimationSet>();
		public SpriteBatch spriteBatch;
		public Texture walkSheet;
	}
	public SirrusAnimation(){
		animated = false;
	}
	
	public SirrusAnimation(AnimationParameters parameters) {
		frameDuration = parameters.frameDuration;
		animationSets = parameters.animationSets;
		walkSheet = parameters.walkSheet;
		spriteBatch = parameters.spriteBatch;
		stateTime = 0f;
	}
	
	public void setAnimationSet(int id){
		for (AnimationSet currSet : animationSets) {
			if(id == currSet.id){
				activeAnimationSetId = id;
				TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth() / 12, walkSheet.getHeight() / 8);
				walkFrames = new TextureRegion[4];
				int index = 0;
				 for (int j = 0; j < 8; j++) {
					 for (int i = 0; i < 12; i++) {
		            	int d = currSet.cols * 3 + currSet.cols;
		            	boolean crt1 = i >= currSet.cols * 4 && i < currSet.cols * 4 + 4;
		            	boolean crt2 = j == currSet.rows * 4 + id;
		            	//if(i == currSet.cols && j >= d && j <= d + 3)
		            	if(crt1 && crt2){
		            		walkFrames[index++] = tmp[j][i];
		            	}
		            }
		        }
				walkAnimation = new Animation(frameDuration, walkFrames);
				//stateTime = 0f;
			}
		}
	}
	float deg = 0;
	public void draw(float delta) {
        stateTime += delta;
        currentFrame = walkAnimation.getKeyFrame(stateTime, true);
        /*Sprite sp = new Sprite(currentFrame);
        sp.setOrigin(sp.getWidth() / 2, sp.getHeight() / 2);
        sp.rotate(deg += delta * 100);*/
        spriteBatch.begin();
        spriteBatch.draw(currentFrame, x, y);
        //sriteBatch.draw(sp, x, y, 0, 0, sp.getWidth(), sp.getHeight(), 1, 1, 0); 
        spriteBatch.end();
    }	
	public void draw(SpriteBatch batch, float delta) {
        stateTime += delta;
        currentFrame = walkAnimation.getKeyFrame(stateTime, true);
        Sprite sp = new Sprite(currentFrame);
        sp.setOrigin(sp.getWidth(), sp.getHeight());
        sp.rotate(deg += delta * 100);
        //batch.begin();
        batch.draw(sp, x, y);
       // batch.end();
    }	
}
