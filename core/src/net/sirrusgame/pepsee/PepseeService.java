package net.sirrusgame.pepsee;

public class PepseeService implements PepseeStatics
{
	public void addObject(Pepsee isometricWorld, PepseeObject obj){
		isometricWorld.isometricObjects.add(obj);
	}
	
	public void removeObject(Pepsee isometricWorld, int identify){
		for (PepseeObject obj : isometricWorld.isometricObjects) {
			if(obj.getIdentify() == identify)
				isometricWorld.isometricObjects.removeValue(obj, true);
		}
	}
	public void removeObject(Pepsee isometricWorld, PepseeObject obj){
		isometricWorld.isometricObjects.removeValue(obj, true);
	}
}
