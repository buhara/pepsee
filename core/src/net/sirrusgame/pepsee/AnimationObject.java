package net.sirrusgame.pepsee;

import com.badlogic.gdx.graphics.g2d.Animation;

public class AnimationObject extends PepseeObject implements PepseeStatics
{
	private Animation animation;

	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}
	
}
