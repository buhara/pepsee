package net.sirrusgame.pepsee;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.IsometricTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

public abstract class PepseeAbstract implements PepseeStatics
{
	protected IsometricTiledMapRenderer renderer;
	public OrthographicCamera camera;
	
	private int[] backgroundLayers;
	private int[] foregroundLayers;
	
	private SpriteBatch defaultSpriteBatch = new SpriteBatch();
	private ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	//private ArrayList<GisoTiledMapTileSet> gisoTiledMapTileSet;
	public Array<PepseeObject> isometricObjects = new Array<PepseeObject>();
	public PepseeObjectComperator isometricObjectComparator = new PepseeObjectComperator();
	private Vector2 screen;
	
	int Tile_W, Tile_H, NumCols, NumRows, vOffset, offsetX, offsetY;
	
	//from Tiled Software 
	public String mapVersion;
	public String mapOrientation;
	public int mapWidth, mapHeight;
	public float mapTileWidth, mapTileHeight;
	
	//first calculates
	protected TiledMap map;
	protected float magicForX, magicForY, bigHorizantal, bigVertical;
	
	public float scale = 1;
	
	public PepseeAbstract(String tmxFile, int[] backgroundLayers, int[] foregroundLayers){
		//gisoTiledMapTileSet = new ArrayList<GisoTiledMapTileSet>();
		map = new TmxMapLoader().load(tmxFile);
		mapOrientation = map.getProperties().get(PepseeStatics.TILED_ORIENTATION).toString();
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(0);	 
		mapWidth = layer.getWidth();
		mapHeight = layer.getHeight();
		mapTileWidth = layer.getTileWidth();
		mapTileHeight = layer.getTileHeight();
		renderer = new IsometricTiledMapRenderer(map);
		this.backgroundLayers = backgroundLayers;
		this.foregroundLayers = foregroundLayers;
		camera = new OrthographicCamera();
		
		setLayerSprites("foreground");
	}
	/** converts point to its coordinates on an isometric grid
	 *  @param point the point to convert
	 *  @param cellWidth the width of the grid cells
	 *  @param cellHeight the height of the grid cells
	 *  @return the given point converted to its coordinates on an isometric grid */
	public static Vector2 toIsometricGridPoint(Vector2 point, float cellWidth, float cellHeight) {
		point.x /= cellWidth;
		point.y = (point.y - cellHeight / 2) / cellHeight + point.x;
		point.x -= point.y - point.x;
		return point;
	}

	/** @see #toIsometricGridPoint(Vector2, float, float) */
	public static Vector2 toIsometricGridPoint(float x, float y, float cellWidth, float cellHeight) {
		return toIsometricGridPoint(new Vector2(x, y), cellWidth, cellHeight);
	}

	/** @see #toIsometricGridPoint(Vector2, float, float) */
	public static Vector3 toIsometricGridPoint(Vector3 point, float cellWidth, float cellHeight) {
		Vector2 vec2 = toIsometricGridPoint(point.x, point.y, cellWidth, cellHeight);
		point.x = vec2.x;
		point.y = vec2.y;
		return point;
	}
	
	public Vector3 screenToIso(Vector3 p) {
		float percentAB = p.y * 100 / Gdx.graphics.getWidth();
		float percentAD = p.x * 100 / Gdx.graphics.getWidth();
		float y = percentAB * mapHeight;
		float x = percentAD * mapWidth;
		float z = p.z;
		return new Vector3(x, y, z);
	}

	public Vector3 toIsometricGridPoint(Vector3 p){
		return toIsometricGridPoint(p, mapTileWidth * scale, mapTileHeight * scale);
	}
	public boolean yaz = true;
	
	int[] cels = new int[] {1, 1};
	 
	public void selectNow(Vector3 touch){
		Vector3 touch2 = touch;
		camera.unproject(touch2);
		Vector3 iso = toIsometricGridPoint(touch2, mapTileWidth * scale, mapTileHeight * scale);
		//Gdx.app.log("asdf", "" + iso);
		int x = (int) iso.x;
		int y = (int) iso.y;
		
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("selector");
		for (int i = 0; i < layer.getWidth(); i++) {
			for (int j = 0; j < layer.getHeight(); j++) {
				Cell cell = new Cell();
				if(i == x && j == y && (x >= 0 && y >= 0)){
					//cell.setTile(map.getTileSets().getTileSet("isometric").getTile(6));
					//Gdx.app.log("asdf", "" + map.getTileSets().getTileSet("isometric").getTile(3).getProperties().get("name"));
					//if(map.getTileSets().getTileSet("isometric").getTile(3).getProperties().get("name").toString().contains("b"))
					//cell.setTile(map.getTileSets().getTileSet("isometric").getTile(3));
				}
				layer.setCell(i, j, cell);
				cels = new int[] {i, j};
			}
		}
		//layer.getCell(0, 0).setTile(map.getTileSets().getTileSet("isometric").getTile(3));
	}
	
	public void logNow(Vector3 touch){
		if(!yaz) return;
		camera.unproject(touch);
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(0);
		for (int i = 0; i < layer.getWidth(); i++) {
			for (int j = 0; j < layer.getHeight(); j++) {
				Cell cell = layer.getCell(i,  j);
				if(i == 2 && j == 2){
					cell.setTile(layer.getCell(0, 0).getTile());
				}
				//Gdx.app.log("val: ", "" + cell.getTile().getProperties().get("name", Float.class));
				//Gdx.app.log("val: ", "" + cell.getTile().getProperties().);
			}
		}
		//Gdx.app.log("touch: ", "" + touch);
		//Gdx.app.log("toIsometricGridPoint: ", "" + toIsometricGridPoint(touch, 64f/1, 32f/1));
		Vector3 iso = toIsometricGridPoint(touch, mapTileWidth * scale, mapTileHeight * scale);
		int x = (int) iso.x;
		int y = (int) iso.y;
		if(x >= 0 && y >= 0)
		layer.getCell(x, y).setTile(layer.getCell(0, 0).getTile());
		yaz = false;
	}
	
	public int[] getCoordinate(Vector3 point, String tileLayerName){
		Vector3 sd = new Vector3(point);
		//camera.unproject(sd);
		//Vector3 iso = toIsometricGridPoint(sd, mapTileWidth * scale, mapTileHeight * scale);
		Vector3 iso = toIsometricGridPoint(new Vector3(point));
		int x = (int) iso.x;
		int y = (int) iso.y;
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(tileLayerName);
		for (int i = 0; i < layer.getWidth(); i++) {
			for (int j = 0; j < layer.getHeight(); j++) {
				if(i == x && j == y && (x >= 0 && y >= 0)){
					if(layer.getCell(i, j) != null){
						//layer.getCell(i, j).setTile(map.getTileSets().getTileSet("isometric").getTile(6));
						return new int[]{i, j};
					}
				}
			}
		}
		return null;
	}
	
	public void blockedPositions(){
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("blockedlayer");
		for (int i = 0; i < layer.getWidth(); i++) {
			for (int j = 0; j < layer.getHeight(); j++) {
					if(layer.getCell(i, j) != null){
						if(layer.getCell(i, j).getTile().getProperties().containsKey("status")){
							boolean crt = (layer.getCell(i, j).getTile().getProperties().get("status").toString().contains("blocked"));
							if(crt){
								Gdx.app.log("test", i + ", " + j);
							}
						}
						
					}
			}
		}
	}
	
	public boolean positionAvailable(PepseeObject obj, Vector3 requestedPosition){
		requestedPosition.y -= mapTileHeight / 2;
		if(obj.getPosition().x < requestedPosition.x){ //soldan yaklaşma
			requestedPosition.x += obj.getWidth() - obj.getOriginX();
		}
		Vector3 iso = toIsometricGridPoint(new Vector3(requestedPosition));
		int x = (int) iso.x;
		int y = (int) iso.y;
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("blockedlayer");
		if(layer.getCell(x, y) != null){
			if(layer.getCell(x, y).getTile().getProperties().containsKey("status")){
				boolean crt = (layer.getCell(x, y).getTile().getProperties().get("status").toString().contains("blocked"));
				if(crt){
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean positionAvailableWithHeight(PepseeObject obj, Vector3 requestedPosition){
		requestedPosition.y += mapTileHeight / 2;
		if(obj.getPosition().x < requestedPosition.x){ //soldan yaklaşma
			requestedPosition.x += obj.getWidth() - obj.getOriginX();
		}else{
			requestedPosition.x -= obj.getWidth() - obj.getOriginX();
		}
		Vector3 iso = toIsometricGridPoint(new Vector3(requestedPosition));
		int x = (int) iso.x;
		int y = (int) iso.y;
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("foreground");
		if(layer.getCell(x, y) != null){
			if(layer.getCell(x, y).getTile().getProperties().containsKey("status")){
				boolean crt = (layer.getCell(x, y).getTile().getProperties().get("status").toString().contains("blocked"));
				boolean crt2 = (layer.getCell(x, y).getTile().getProperties().get("status").toString().contains("platform"));
				if(crt){
					return false;
				}
				if(crt2){
					obj.setActualAltitude(Float.valueOf(layer.getCell(x, y).getTile().getProperties().get("height").toString()));
					return true;
				}else{
					obj.setActualAltitude(0.0f);
					return true;
				}
			}
		}else{
			obj.setActualAltitude(0.0f);
		}
		return true;
	}
	
	public void updateIsometricObjectsInfos(){
		for (PepseeObject obj : isometricObjects) {
			Vector3 isoTile = new Vector3(obj.getPosition().x, obj.getPosition().y, obj.getPosition().z);
			int[] val = getCoordinate(new Vector3(isoTile), "blockedlayer");
			if(val != null){
				obj.setActiveCellCoordinateX(val[0]);
				obj.setActiveCellCoordinateY(val[1]);
			}
		}
	}
	
	public void setLayerSprites(String layerName){
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(layerName);
		for (int i = 0; i < layer.getWidth(); i++) {
			for (int j = 0; j < layer.getHeight(); j++) {
				if(layer.getCell(i, j) != null){
					Cell cell = layer.getCell(i, j);
					int srcX = i * 32 + (j * 32);
					int srcY = j * 32 / 2 - (i * 16);
					Sprite sp = new Sprite(cell.getTile().getTextureRegion());
					sp.setPosition(srcX, srcY);
					//sp.setSize(64, 64);
					sprites.add(sp);
					SpriteObject so = new SpriteObject();
					so.setSprite(sp);
					so.setPosition(new Vector3(srcX, srcY, 0));
					so.setSprite(sp);
					isometricObjects.add(so);
				}
			}
		}
	}
	
	public void render(float delta){	
		renderer.setView(camera);
		renderer.getSpriteBatch().setProjectionMatrix(camera.combined);
		renderer.render(backgroundLayers);
		renderer.render(foregroundLayers);
		if(isometricObjects.size > 0)
			isometricObjects.sort(isometricObjectComparator);
		renderer.getSpriteBatch().begin();
		for (PepseeObject obj : isometricObjects) {
			if(obj.getSprite() != null){
				obj.getSprite().draw(renderer.getSpriteBatch());
			}
				
			if(obj.isAnimation()){
				Vector3 likeTouchPoint = getLikeTouchPoint(obj.getPosition());
				selectNow(likeTouchPoint);
				obj.setActiveCellCoordinateX((int) likeTouchPoint.x);
				obj.setActiveCellCoordinateY((int) likeTouchPoint.y);
				obj.setPositionLikeTouchPoint(likeTouchPoint);
				obj.getSirrusAnimation().draw((SpriteBatch)renderer.getSpriteBatch(), delta);
			}
		}
		renderer.getSpriteBatch().end();
		renderer.render(new int[]{2});
	}
	
	private Vector3 getLikeTouchPoint(Vector3 pos){
		return new Vector3(pos.x, Gdx.graphics.getHeight() - pos.y, pos.z);
	}
	
	public void update(float delta)
	{
		updateIsometricObjectsInfos();
		camera.update();
	}
	
	public void resize(int width, int height)
	{
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		camera.update();		
	}	
}
