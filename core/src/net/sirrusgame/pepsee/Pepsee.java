package net.sirrusgame.pepsee;

public class Pepsee extends PepseeAbstract implements PepseeStatics
{
	public PepseeService service = new PepseeService();

	public Pepsee(String tmxFile, int[] backgroundLayers, int[] foregroundLayers) 
	{
		super(tmxFile, backgroundLayers, foregroundLayers);
	}

	public void update(float delta)
	{
		super.update(delta);
	}
	
	public void draw(float delta)
	{
		renderer.setView(camera);
		render(delta);
	}
	
	public void resize(int width, int height) 
	{
		super.resize(width, height);
	}
	
}
