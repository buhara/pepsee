package net.sirrusgame.pepsee;

public interface PepseeStatics
{
	public final String TILED_VERSION = "version";
	public final String TILED_ORIENTATION = "orientation";
	public final String TILED_FIRSTGID = "firstgid";
	public final String TILED_IMAGESOURCE = "imagesource";
	public final String TILED_WIDTH = "width";
	public final String TILED_HEIGHT = "height";
	public final String TILED_TILEWIDTH = "tilewidth";
	public final String TILED_TILEHEIGHT = "tileheight";
	public final String TILED_ORTHOGONAL = "orthogonal";
	public final String TILED_ISOMETRIC = "isometric";
	public final String TILED_STAGGERED = "staggered";	
}
