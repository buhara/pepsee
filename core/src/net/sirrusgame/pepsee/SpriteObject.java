package net.sirrusgame.pepsee;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class SpriteObject extends PepseeObject implements PepseeStatics
{
	private Sprite sprite;

	public Sprite getSprite() {
		return sprite;
	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}
	
}
