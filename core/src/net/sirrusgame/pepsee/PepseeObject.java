package net.sirrusgame.pepsee;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector3;

public abstract class PepseeObject implements PepseeStatics
{
	private int identify;
	private float width, height, originX = 0, originY = 0, actualAltitude = 0;
	private float isoDepth = 0;
	private Vector3 positionShadow = new Vector3();
	private Vector3 position = new Vector3();
	private Vector3 positionLikeTouchPoint = new Vector3();
	private Vector3 positionIsometric = new Vector3();
	private int activeCellCoordinateX, activeCellCoordinateY;
	private SirrusAnimation sirrusAnimation;
	private Sprite sprite;
	private boolean animation;
	
	public Vector3 getPosition() {
		return position;
	}

	public void setPosition(Vector3 position) {
		this.position = position;
		updateIsoDepth();
	}

	public float getIsoDepth() {
		return isoDepth;
	}
	
	private void updateIsoDepth() {
		isoDepth = position.x + position.y;
	}

	public Vector3 getPositionIsometric() {
		return positionIsometric;
	}

	public void setPositionIsometric(Vector3 positionIsometric) {
		this.positionIsometric = positionIsometric;
	}

	public float getHeight() {
		return height;
	}

	public PepseeObject setHeight(float height) {
		this.height = height;
		return this;
	}

	public float getWidth() {
		return width;
	}

	public PepseeObject setWidth(float width) {
		this.width = width;
		return this;
	}

	public int getIdentify() {
		return identify;
	}

	public void setIdentify(int identify) {
		this.identify = identify;
	}
	
	public int getActiveCellCoordinateX() {
		return activeCellCoordinateX;
	}

	public void setActiveCellCoordinateX(int activeCellCoordinateX) {
		this.activeCellCoordinateX = activeCellCoordinateX;
	}
	public int getActiveCellCoordinateY() {
		return activeCellCoordinateY;
	}

	public void setActiveCellCoordinateY(int activeCellCoordinateY) {
		this.activeCellCoordinateY = activeCellCoordinateY;
	}

	public Vector3 getPositionLikeTouchPoint() {
		return positionLikeTouchPoint;
	}

	public void setPositionLikeTouchPoint(Vector3 positionLikeTouchPoint) {
		this.positionLikeTouchPoint = positionLikeTouchPoint;
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}

	public SirrusAnimation getSirrusAnimation() {
		return sirrusAnimation;
	}

	public void setSirrusAnimation(SirrusAnimation sirrusAnimation) {
		this.sirrusAnimation = sirrusAnimation;
	}

	public boolean isAnimation() {
		return animation;
	}

	public void setAnimation(boolean animation) {
		this.animation = animation;
	}

	public float getOriginX() {
		return originX;
	}

	public PepseeObject setOriginX(float originX) {
		this.originX = originX;
		return this;
	}

	public float getOriginY() {
		return originY;
	}

	public PepseeObject setOriginY(float originY) {
		this.originY = originY;
		return this;
	}

	public float getActualAltitude() {
		return actualAltitude;
	}

	public void setActualAltitude(float actualAltitude) {
		this.actualAltitude = actualAltitude;
	}

	public Vector3 getPositionShadow() {
		return positionShadow;
	}

	public void setPositionShadow(Vector3 positionShadow) {
		this.positionShadow = positionShadow;
	}

}
