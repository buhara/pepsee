package net.sirrusgame.pepsee;

import java.util.Comparator;

public class PepseeObjectComperator  implements Comparator<PepseeObject>, PepseeStatics {
/*
	@Override
	public int compare (Entity a, Entity b) {
		if(a.getIsoDepth() > b.getIsoDepth()){
			return -1;
		}else if(a.getIsoDepth() < b.getIsoDepth()){
			return 1;
		}
		return 0;
		//return (e2.getPosition().z - e1.getPosition().z) > 0 ? -1 : 1;
	}
	
	@Override
	public int compare (IsometricObject a, IsometricObject b) {
		return (a.getPositionIsometric().y - b.getPositionIsometric().y) > 0 ? -1 : 1;
	}
	*/
	@Override
	public int compare (PepseeObject a, PepseeObject b) {
		if ((a.getPosition().y - b.getPosition().y) > 0){
			return -1;
		}else if ((a.getPosition().y - b.getPosition().y) < 0){
			return 1;
		}
		return -1;
	}
	
}
