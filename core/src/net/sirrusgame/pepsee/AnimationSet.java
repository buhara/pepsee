package net.sirrusgame.pepsee;

public class AnimationSet
{
	public int id;
	public int cols;
	public int rows;
	
	public AnimationSet(int id, int rows, int cols) {
		this.id = id;
		this.cols = cols;
		this.rows = rows;
	}
}
