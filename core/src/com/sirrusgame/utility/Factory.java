package com.sirrusgame.utility;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class Factory 
{

	public class ToggleButton{
		public TextButton textButton;
		public boolean active = false;
		public String ident = "";
		public String group = "";
		public boolean toggleStatus = true;
		
		public ToggleButton(TextButton textButton, String group, String ident, boolean toggleStatus, boolean active) {
			this.textButton = textButton;
			this.ident = ident;
			this.group = group;
			this.toggleStatus = toggleStatus;
			this.active = active;
			if(active){
				this.active = false;
				this.active = click();
			}
		}
		public boolean click(){
			Skin skin = new Skin();
			TextButtonStyle oldStyle = textButton.getStyle();
			TextButtonStyle style = new TextButtonStyle();
			style.up = oldStyle.down;
			style.down = oldStyle.up;
			style.checked = oldStyle.down;
			style.font = new BitmapFont();
			textButton.setStyle(style);
			//if(toggleStatus){
				if(active){
					active = false;
				}else{
					active = true;
				}
			//}
			return active;
		}
	}
	
	public ToggleButton getTextButton(String group, String ident, TextureAtlas atlas, String up,
			String down, Vector2 pos, Vector2 size, EventListener eventListener, boolean toggleStatus, boolean active) {
		TextButton button = getClickButton(atlas, up, down, pos, size, eventListener);
		return new ToggleButton(button, group, ident, toggleStatus, active);
		
	}
	
	public TextButton getClickButton(TextureAtlas atlas, String up,
			String down, Vector2 pos, Vector2 size, EventListener eventListener) {
		Skin skin = new Skin();
		skin.addRegions(atlas);
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable(up);
		style.down = skin.getDrawable(down);
		style.checked = skin.getDrawable(up);
		style.font = new BitmapFont();
		TextButton button = new TextButton("", style);
		button.addListener(eventListener);
		button.setPosition(pos.x, pos.y);
		button.setSize(size.x, size.y);
		return button;
	}
}
