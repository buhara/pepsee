package com.sirrusgame.utility;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.sirrusgame.game.C;
import com.sirrusgame.sirrusgamebase.SirrusScreen;

public class Typing {

	private SpriteBatch mSpriteBatch = new SpriteBatch();
	private BitmapFont mBitmapFont;// = new BitmapFont(Gdx.files.local("fonts/unispace.fnt"));
	//DejaVuSans
	FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/arial.ttf"));
	FreeTypeFontParameter parameter = new FreeTypeFontParameter();
	
	public Typing(SirrusScreen screen) {
		//mBitmapFont = screen.getGame().getSirrusAssets().getManager().get(C.ASSET_FONT_UNISPACE, BitmapFont.class);
		parameter.size = 12;
		mBitmapFont = generator.generateFont(parameter); // font size 12 pixels
		generator.dispose();
	}
	public void render(String text, int x, int y, float scale){
		mBitmapFont.setScale(scale);
		mBitmapFont.setColor(0, 0, 0, 1); 
		mSpriteBatch.begin();
		mBitmapFont.draw(mSpriteBatch, text, x, y);
		mSpriteBatch.end();
	}
}
