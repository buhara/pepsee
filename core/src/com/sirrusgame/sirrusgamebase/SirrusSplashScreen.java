package com.sirrusgame.sirrusgamebase;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.sirrusgame.inputprocessors.SplashInputProcessor;

public class SirrusSplashScreen  extends SirrusScreen
{
	OrthographicCamera cam;
	BitmapFont font12;
	SpriteBatch batch;
	Texture bgImage;
	int centerX, centerY;
	Sprite sprite;
	
	
	public void setSplashScreenfileName(String splashScreenfileName){
		this.splashScreenfileName = splashScreenfileName;
	}
	
	@Override
	public void create() {
		bgImage = new Texture(Gdx.files.internal(this.splashScreenfileName));
		bgImage.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		TextureRegion region = new TextureRegion(bgImage, 0, 0, 1024, 578);
		sprite = new Sprite(region);
		sprite.setAlpha(0);
		sprite.setSize(1f, 1f * sprite.getHeight() / sprite.getWidth());
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2);

		centerX = Gdx.graphics.getWidth() / 2;
        centerY = Gdx.graphics.getHeight() / 2;
        
        float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		cam = new OrthographicCamera(1, h/w);

		batch = new SpriteBatch();
		tweenTo("UP", 1, 0, 0, 1f);
		this.getInputMultiplexer().addProcessor(new SplashInputProcessor(this));
	}
	
	@Override
	public void update(float delta) {
		sprite.setAlpha(this.getTweenShadow().values.x);
	}
	
	@Override
	public void draw(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		cam.update();
		batch.setProjectionMatrix(cam.combined);
		batch.begin();
		sprite.draw(batch);
		batch.end();
	}

	@Override
	public void setGame(SirrusGame game) {
		this.game = game;		
	}

}
