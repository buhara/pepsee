package com.sirrusgame.sirrusgamebase;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import com.sirrusgame.sirrusgamebase.objects.SirrusTweenAccessor;
import com.sirrusgame.sirrusgamebase.objects.TweenShadow;

public abstract class SirrusScreen implements Screen {

	public abstract void create();
	public abstract void update (float delta);
	public abstract void draw (float delta);
	public abstract void setGame(SirrusGame game);
	
	private boolean screenDone = false;
	private TweenManager manager;
	private TweenCallback cb;
	public boolean tweenDone = true;
	private TweenShadow tweenShadow = new TweenShadow();
	protected String splashScreenfileName;
	public SirrusGame game;
	private InputMultiplexer inputMultiplexer;
	private Stage stage;
	public SpriteBatch spriteBatch;
	
	public SirrusScreen() {
		Tween.registerAccessor(TweenShadow.class, new SirrusTweenAccessor());
		manager = new TweenManager();
		inputMultiplexer = new InputMultiplexer();
		Gdx.input.setInputProcessor(inputMultiplexer);
		spriteBatch = new SpriteBatch();
	}

	public void tweenTo(String dir, float val1, float val2, float val3, float time)
	{
		if(tweenDone == false) return;
		tweenDone = false;
		
		cb= new TweenCallback() {
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				tweenCompleted(type);				
			}
		};
		
		if(dir == "UP"){
			Tween.to(tweenShadow, SirrusTweenAccessor.ALPHA, time)
				.target(val1, val2, val3)
				.ease(TweenEquations.easeInQuad)
				.repeatYoyo(0, 0.2f)
				.setCallback(cb)
				.setCallbackTriggers(TweenCallback.COMPLETE)
				.start(manager);
		}
	}
	
	protected void tweenCompleted(int type) {
		tweenDone = true;
		//if(this.getGame().getCurrScreenId() == C.SCREEN_SPLASH)
		//	this.getGame().setSirrusScreen(C.SCREEN_MENU);
	}

	@Override
	public void render(float delta) {
		manager.update(delta);
		update(delta);
		draw(delta);
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		create();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	public boolean isScreenDone() {
		return screenDone;
	}

	public void setScreenDone(boolean screenDone) {
		this.screenDone = screenDone;
	}

	public TweenShadow getTweenShadow() {
		return tweenShadow;
	}

	public void setTweenShadow(TweenShadow tweenShadow) {
		this.tweenShadow = tweenShadow;
	}
	public String getSplashScreenfileName() {
		return splashScreenfileName;
	}
	public void setSplashScreenfileName(String splashScreenfileName) {
		this.splashScreenfileName = splashScreenfileName;
	}
	public SirrusGame getGame() {
		return game;
	}
	public Stage getStage() {
		return stage;
	}
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	public InputMultiplexer getInputMultiplexer() {
		return inputMultiplexer;
	}
	
	
}
