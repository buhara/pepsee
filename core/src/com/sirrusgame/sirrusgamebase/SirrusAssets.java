package com.sirrusgame.sirrusgamebase;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class SirrusAssets 
{

	private AssetManager manager;
	
	public void loadTexture(String fileName, Class<Texture> type){
		manager.load(fileName, type);
	}
	public void unLoad(int screen){
		;
	}
	
	public SirrusAssets() {
		manager = new AssetManager();
	} 
	
	public void loadTextureAtlas(String fileName, Class<TextureAtlas> type){
		manager.load(fileName, type);
	}
	
	public AssetManager getManager() {
		return manager;
	}
	public void setManager(AssetManager manager) {
		this.manager = manager;
	}
	
}
