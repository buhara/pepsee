package com.sirrusgame.sirrusgamebase;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.sirrusgame.utility.Factory;
import com.sirrusgame.utility.Factory.ToggleButton;

public class SirrusMenuScreen extends SirrusScreen {

	
	private OrthographicCamera camera;
	private Stage stage;
	private Table table;
	private Factory factory;
	ArrayList<ToggleButton> toggleButtons = new ArrayList<Factory.ToggleButton>();
	
	public SirrusMenuScreen() {;
	}
	@Override
	public void create() {
		factory = new Factory();
		stage = new Stage();
		table = new Table();
		table.row();
		table.debug(); 
		table.debugTable();
		table.setFillParent(true);
		stage.addActor(table);
		
	}

	@Override
	public void update(float delta) {

	}

	@Override
	public void draw(float delta) {
		stage.act(delta);
		stage.draw();
		Table.drawDebug(stage);
	}
	@Override
	public void setGame(SirrusGame game) {
		this.game = game;
	}

}
