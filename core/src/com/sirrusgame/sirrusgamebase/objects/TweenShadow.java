package com.sirrusgame.sirrusgamebase.objects;

import com.badlogic.gdx.math.Vector3;

public class TweenShadow 
{
	public Vector3 values = new Vector3();
	
	public void setX(float x){
		this.values.x = x;
	}
	public void setY(float y){
		this.values.y = y;
	}
	public void setZ(float z){
		this.values.z = z;
	}
}
