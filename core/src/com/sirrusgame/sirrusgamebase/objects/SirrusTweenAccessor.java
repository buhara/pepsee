package com.sirrusgame.sirrusgamebase.objects;

import aurelienribon.tweenengine.TweenAccessor;

public class SirrusTweenAccessor implements TweenAccessor<TweenShadow> 
{
	
	public static final int ALPHA = 1;

	@Override
	public int getValues(TweenShadow tw, int tweenType, float[] returnValues) {
		switch(tweenType){
		case ALPHA: 
			returnValues[0] = tw.values.x;
			returnValues[1] = tw.values.y;
			returnValues[2] = tw.values.z;
			return 3;
		default: assert false; return -1;
		}
	}

	@Override
	public void setValues(TweenShadow tw, int tweenType, float[] newValues) {
		switch(tweenType){
		case ALPHA:
			tw.setX(newValues[0]);
			tw.setY(newValues[1]);
			tw.setZ(newValues[2]);
			break;
		default: assert false; break;
		}
	}
}
