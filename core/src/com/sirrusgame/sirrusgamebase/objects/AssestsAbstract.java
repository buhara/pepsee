package com.sirrusgame.sirrusgamebase.objects;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public abstract class AssestsAbstract  extends Game  
{

	private AssetManager manager;
	
	public abstract void load(int screen);
	public abstract void unLoad(int screen);
	
	public AssestsAbstract() {
		manager = new AssetManager();
	} 
	
	public void loadAtlas(String fileName, Class<TextureAtlas> type){
		manager.load(fileName, type);
	}
	
	public AssetManager getManager() {
		return manager;
	}
	
	
}
