package com.sirrusgame.sirrusgamebase.objects;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.ObjectMap;
import com.sirrusgame.vaguefortune.screens.GameScreen;

public class SirrusScreenDescription
{
	private int screenId;
	private int nextScreenId;
	private Class screen;
	private final ObjectMap<String, Class> assets = new ObjectMap<String, Class>();
	private boolean isSplashScreen = false;
	private String fileName;
	
	public SirrusScreenDescription(){
	}
	
	public SirrusScreenDescription setId(int screenId){
		this.screenId = screenId;
		return this;
	}
	public int getId(){
		return this.screenId;
	}
	
	public <T> SirrusScreenDescription(Class<T> screenClass){
		this.screen = screenClass;
	}
	
	public SirrusScreenDescription addAssets(String fileName){
		this.fileName = fileName;
		return this;
	}
	
	public synchronized <T> SirrusScreenDescription addAssets(String fileName, Class<T> type){
		assets.put(fileName, type);
		return this;
	}


	public int getScreenId() {
		return screenId;
	}


	public <T> SirrusScreenDescription setScreen(Class<T> screenClass) {
		this.screen = screenClass;
		return this;
	}
	
	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}


	public Screen getScreen() {
		Class s = (Class) this.screen;
		Screen sc;
		try {
			sc = (Screen) s.newInstance();
			return sc;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}

/*
	public void setScreen(Screen screen) {
		this.screen = screen;
	}
*/

	public ObjectMap<String, Class> getAssets() {
		return assets;
	}

	public boolean isSplashScreen() {
		return isSplashScreen;
	}

	public SirrusScreenDescription setSplashScreen(boolean isSplashScreen) {
		this.isSplashScreen = isSplashScreen;
		return this;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getNextScreenId() {
		return nextScreenId;
	}

	public SirrusScreenDescription setNextScreenId(int nextScreenId) {
		this.nextScreenId = nextScreenId;
		return this;
	}
	
	
}
