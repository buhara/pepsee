package com.sirrusgame.sirrusgamebase;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class SirrusLoadScreen extends SirrusScreen{
	SpriteBatch batch;
	OrthographicCamera camera;
	ProgressBar progressBar;
	Skin skin;
	ShapeRenderer shapeRenderer;
	
	public SirrusLoadScreen() {	
	}
	
	@Override
	public void create() {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();		
		camera = new OrthographicCamera(1, h/w);
		skin = new Skin();
		skin.add("knob_bg", new Texture(Gdx.files.internal("graphs/knob_bg.png")));
		skin.add("knob", new Texture(Gdx.files.internal("graphs/knob.png")));
        skin.add("knob_after", new Texture(Gdx.files.internal("graphs/knob_after.png")));
        
		ProgressBar.ProgressBarStyle style = new ProgressBar.ProgressBarStyle();
		style.background = skin.getDrawable("knob");
		style.knob = skin.getDrawable("knob");
		//style.knobBefore = skin.getDrawable("knob_after");
		
		progressBar = new ProgressBar(0, 1, 0.1f, false, style);
		
		game.loadAssets(this.game.getCurrScreenId());
		//Assets.load(getGame().getCurrentGameScreen());
		batch = new SpriteBatch();
		progressBar.setVisible(true);
		//progressBar.setOrigin(100, 0);
		progressBar.setSize(100f/870f, 120f/480f);
		progressBar.setPosition((Gdx.graphics.getWidth()/2) - 256f/2f, (Gdx.graphics.getHeight()/2));
		//progressBar.setPosition(0.5f, 0.5f);
		progressBar.setValue(0.3f);
		
		shapeRenderer = new ShapeRenderer();
	}

	float globalPercent = 0;
	
	@Override
	public void update (float delta) 
	{
		float last = 0.27f;
		float curr = 
		globalPercent = last * game.getSirrusAssets().getManager().getProgress();
		//Gdx.app.log("manager.getProgress()", progressBar.getValue() + "");
		//progressBar.setValue(Assets.manager.getProgress());
		if(game.getSirrusAssets().getManager().getProgress() >= 1f){
			game.setNextSirrusScreen(game.getCurrScreenId());
		}

	}
	
	@Override
	public void draw(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(1f / 255f, 48f / 255f, 102f / 255f, 1);
		game.getSirrusAssets().getManager().update();
		camera.update();
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(0, 192f / 255f, 1, 1);
		shapeRenderer.rect(-0.13f, 0, globalPercent, 0.01f);
		shapeRenderer.end();
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(1, 1, 1, 1);
		shapeRenderer.rect(-0.13f, 0, 0.27f, 0.01f);
		shapeRenderer.end();
		// batch.setProjectionMatrix(camera.combined);
		batch.begin();
		// progressBar.draw(batch, 1);
		batch.end();
	}

	@Override
	public void resize(int width, int height){
		super.resize(width, height);
		progressBar.setSize(100f/870f, 120f/480f);
	}
	
	@Override
	public void dispose(){
		super.dispose();
		skin.dispose();
	}

	@Override
	public void setGame(SirrusGame game) {
		this.game = game;
	}

}
