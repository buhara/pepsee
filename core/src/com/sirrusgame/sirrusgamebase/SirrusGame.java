package com.sirrusgame.sirrusgamebase;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.sirrusgame.sirrusgamebase.objects.SirrusScreenDescription;

public abstract class SirrusGame extends Game {

	private int currScreenId;
	private int nextScreenId;
	private final ArrayList<SirrusScreenDescription> sirrusScreenDescriptions = new ArrayList<SirrusScreenDescription>();
	private SirrusAssets sirrusAssets = new SirrusAssets();
	
	public abstract void setSirrusScreens();
	
	public SirrusGame(){
		this.setSirrusScreens();
	}

	public void loadAssets(int screenId){
		SirrusScreenDescription desc = getDescriptionFromId(screenId);	
		System.out.println(desc.getId());
		ObjectMap<String, Class> assets = desc.getAssets();
		for (Entry<String, Class> entry : assets) {
			sirrusAssets.loadTexture(entry.key, entry.value);
		}
		
	}
	public void setNextSirrusScreen(int id){
		SirrusScreenDescription desc = getDescriptionFromId(id);
		setSirrusScreen(desc.getNextScreenId());
	}
	public void setSirrusScreen(int id){
		SirrusScreenDescription desc = getDescriptionFromId(id);
		SirrusScreen sc = (SirrusScreen) desc.getScreen();
		this.currScreenId = id;
		sc.setGame(this);
		if(desc.isSplashScreen()){
			sc.setSplashScreenfileName(desc.getFileName());
		}
		//loadAssets(id);
		super.setScreen(sc);
		sc.setGame(this); //abstract
	}
	public void addSirrusScreenDescriptions(SirrusScreenDescription screen) {
		sirrusScreenDescriptions.add(screen);
	}

	public ArrayList<SirrusScreenDescription> getSirrusScreenDescriptions() {
		return sirrusScreenDescriptions;
	}
	
	public SirrusScreenDescription getDescriptionFromId(int id){
		for (SirrusScreenDescription desc : sirrusScreenDescriptions) {
			if(desc.getId() == id){
				return desc;
			}
		}
		return null;
	}

	public int getCurrScreenId() {
		return currScreenId;
	}

	public void setCurrScreenId(int currScreenId) {
		this.currScreenId = currScreenId;
	}

	public SirrusAssets getSirrusAssets() {
		return sirrusAssets;
	}

	public void setSirrusAssets(SirrusAssets sirrusAssets) {
		this.sirrusAssets = sirrusAssets;
	}

	public int getNextScreenId() {
		return nextScreenId;
	}

	public void setNextScreenId(int nextScreenId) {
		this.nextScreenId = nextScreenId;
	}
	
	
}
