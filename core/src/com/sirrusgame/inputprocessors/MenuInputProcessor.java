package com.sirrusgame.inputprocessors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Keys;
import com.sirrusgame.game.C;
import com.sirrusgame.sirrusgamebase.SirrusScreen;

public class MenuInputProcessor implements InputProcessor {

	SirrusScreen screen;
	
	public MenuInputProcessor(SirrusScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Keys.X)
			this.screen.getGame().setSirrusScreen(C.SCREEN_SPLASH);
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		if(character == Keys.X)
			this.screen.getGame().setSirrusScreen(C.SCREEN_MENU);
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		this.screen.getGame().setSirrusScreen(C.SCREEN_LOADING);
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
