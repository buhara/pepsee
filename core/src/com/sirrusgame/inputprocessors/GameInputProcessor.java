package com.sirrusgame.inputprocessors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.sirrusgame.game.C;
import com.sirrusgame.sirrusgamebase.SirrusScreen;
import com.sirrusgame.vaguefortune.entities.Human;
import com.sirrusgame.vaguefortune.screens.GameScreen;

public class GameInputProcessor implements InputProcessor {

	GameScreen screen;
	float velocity = 10f;
	
	public GameInputProcessor(GameScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Keys.X)
			this.screen.getGame().setSirrusScreen(C.SCREEN_MENU);
		screen.walking = false;
		if(keycode == Keys.LEFT){
			screen.directionX = -1;
			screen.vertical = false;
			screen.walking = true;
		}else if(keycode == Keys.RIGHT){
			screen.directionX = 1;
			screen.vertical = false;
			screen.walking = true;
		}else if(keycode == Keys.UP){
			screen.directionY = 1;
			screen.vertical = true;
			screen.walking = true;
		}else if(keycode == Keys.DOWN){
			screen.directionY = -1;
			screen.vertical = true;
			screen.walking = true;
		}else{
			screen.walking = false;
		}
		/*
		if(keycode == Keys.NUM_1) screen.human.changeAnimationSet(Human.WALK_LEFT);
		if(keycode == Keys.NUM_2) screen.human.changeAnimationSet(Human.WALK_RIGHT);
		if(keycode == Keys.NUM_3) screen.human.changeAnimationSet(Human.WALK_UP);
		if(keycode == Keys.NUM_4) screen.human.changeAnimationSet(Human.WALK_DOWN);
		if(keycode == Keys.NUM_0) screen.human.setHumanNo(0);
		if(keycode == Keys.NUM_1) screen.human.setHumanNo(1);
		if(keycode == Keys.NUM_2) screen.human.setHumanNo(2);
		if(keycode == Keys.NUM_3) screen.human.setHumanNo(3);
		if(keycode == Keys.NUM_4) screen.human.setHumanNo(4);
		if(keycode == Keys.NUM_5) screen.human.setHumanNo(5);
		if(keycode == Keys.NUM_6) screen.human.setHumanNo(6);
		if(keycode == Keys.NUM_7) screen.human.setHumanNo(7);*/
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		screen.walking = false;
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		//screen.touch = new Vector3(screenX, screenY, 0);
		screen.touched(new Vector3(screenX, screenY, 0));
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		screen.dragging  = false;
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		screen.dragging  = true;
		Vector3 v = new Vector3(screenX, screenY, 0);
		//camera.unproject(v);
		screen.touch = new Vector3(screenX, screenY, 0);
		//screen.coordinateSystem = new Vector2(screenX - screen.zero.x,  screen.zero.y - screenY);
		//Gdx.app.log("dragg", coordinateSystem.y + "");
		screen.selectNow(new Vector3(screenX, screenY, 0));
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		//screen.touch = new Vector3(screenX, screenY, 0);
		//screen.selectNow(new Vector3(screenX, screenY, 0));
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
