package com.sirrusgame.vaguefortune.entities;

import net.sirrusgame.pepsee.AnimationSet;
import net.sirrusgame.pepsee.PepseeObject;
import net.sirrusgame.pepsee.SirrusAnimation;
import net.sirrusgame.pepsee.SirrusAnimation.AnimationParameters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.sirrusgame.game.C;
import com.sirrusgame.sirrusgamebase.SirrusScreen;

public class Human extends PepseeObject {

	public static final int WALK_DOWN = 0;
	public static final int WALK_LEFT = 1;
	public static final int WALK_RIGHT = 2;
	public static final int WALK_UP = 3;
	
	public int row = 0, coll = 0;
	
	SirrusAnimation.AnimationParameters param;
	
	//public SirrusAnimation animation;
	SirrusScreen screen;
	
	public Human(SirrusScreen screen, Vector3 pos, int row, int coll) {
		this.screen = screen;
		this.row = row;
		this.row = coll;
		changeAnimationSet(WALK_DOWN, row,  coll);
		setPosition(pos);
		setAnimation(true);
	}
	
	public Human(SirrusScreen screen, int row, int coll) {
		this.screen = screen;
		this.row = row;
		this.row = coll;
		changeAnimationSet(WALK_DOWN, row,  coll);
		setAnimation(true);
	}
	
	public Human(SirrusScreen screen) {
		this.screen = screen;
		changeAnimationSet(WALK_DOWN, row,  coll);
		/*final TextureAtlas atlas = Assets.manager.get(C.ASSET_TEXTURE_ATLAS, TextureAtlas.class);
		 * public FileHandle fileHandle;
		public float frameDuration;
		public ArrayList<AnimationSet> animationSets = new ArrayList<AnimationSet>();
		public SpriteBatch spriteBatch;
		public Texture walkSheet;*/
		setAnimation(true);
	}
	
	public void changeAnimationSet(int setId, int row, int coll){
		param = new AnimationParameters();
		param.animationSets.add(new AnimationSet(WALK_DOWN, row, coll));
		param.animationSets.add(new AnimationSet(WALK_LEFT, row, coll));
		param.animationSets.add(new AnimationSet(WALK_RIGHT, row, coll));
		param.animationSets.add(new AnimationSet(WALK_UP, row, coll));
		param.spriteBatch = screen.spriteBatch;
		param.walkSheet = screen.getGame().getSirrusAssets().getManager().get(C.ASSET_TEXTURE_CHARACTERS, Texture.class);
		param.frameDuration = 0.15f;
		SirrusAnimation animation = new SirrusAnimation(param);
		animation.x = getPosition().x;
		animation.y = getPosition().y;
		animation.setAnimationSet(setId);
		setSirrusAnimation(animation);
	}
	
	public void updateAnimationSet(int setId){
		getSirrusAnimation().setAnimationSet(setId);
	}
	
	@Override
	public void setPositionShadow(Vector3 pos){
		super.setPositionShadow(pos);
		super.setPosition(pos);
		getSirrusAnimation().x = getPosition().x;
		getSirrusAnimation().y = getPosition().y + getActualAltitude() + getHeight() / 2;
	}
	@Override
	public void setPosition(Vector3 pos){
		super.setPosition(pos);
		getSirrusAnimation().x = getPosition().x;
		getSirrusAnimation().y = getPosition().y;
	}
}
