package com.sirrusgame.vaguefortune.screens;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.sirrusgame.inputprocessors.GameInputProcessor;
import com.sirrusgame.sirrusgamebase.SirrusGame;
import com.sirrusgame.sirrusgamebase.SirrusScreen;
import com.sirrusgame.utility.Typing;
import com.sirrusgame.vaguefortune.entities.Human;
import net.sirrusgame.pepsee.PepseeAbstract;
import net.sirrusgame.pepsee.PepseeObjectComperator;
import net.sirrusgame.pepsee.PepseeObject;
import net.sirrusgame.pepsee.Pepsee;

public class GameScreen extends SirrusScreen {

	public Pepsee pepsee;
	FPSLogger fps = new FPSLogger();
	public SpriteBatch batch;
	public Texture img;
	//public OrthographicCamera camera;
	public Vector2 zero, coordinateSystem;
	public boolean dragging  = false;
	
	public Vector3 touch = new Vector3();
	public Vector3 touch2 = new Vector3();
	
	//public ArrayList<Human> humans = new ArrayList<Human>();
	public Array<Human> humans = new Array<Human>();
	Typing typer;
	
	public GameScreen(){
	}
	
	@Override
	public void create() {		
		pepsee = new Pepsee("maps/main2.tmx", new int[]{0}, new int[]{1});
		batch = new SpriteBatch();
		typer = new Typing(this);
		//camera = new OrthographicCamera();
		//camera.position.set(0, 0, 0);  
		this.getInputMultiplexer().addProcessor(new GameInputProcessor(this));
		coordinateSystem = new Vector2(0, 0);
		touch = new Vector3();
		pepsee.camera.translate(pepsee.scale * pepsee.mapTileWidth * pepsee.mapHeight / 2, pepsee.scale * pepsee.mapTileHeight / 2);
		//pepsee.blockedPositions();
	}
	
	public void addHuman(float x, float y){
		//selectNow(new Vector3(x, y, 0));
		Random randomGenerator = new Random();
		int row = randomGenerator.nextInt(2);
		int coll = randomGenerator.nextInt(3);
		Vector3 pos = pepsee.camera.unproject(new Vector3(x, y, 0));
		Human human = new Human(this, pos, row, coll);	
		human.setWidth(32).setHeight(48).setOriginX(16).setOriginY(0);
		if(pepsee.positionAvailableWithHeight(human, new Vector3(pos)))
			human.setPositionShadow(pos);
		getTweenShadow().values.x = pos.x;
		getTweenShadow().values.y = pos.y;
		humans.add(human);
		pepsee.service.addObject(pepsee, human);
		//humans.sort(pepsee.isometricObjectComparator);
	}
	
	public boolean vertical = false;
	public float directionY = 1;
	public float directionX = 1;
	
	public void touched(Vector3 touch){
		/*if(touch.y > Gdx.graphics.getHeight() / 2) directionY = 1;
		if(touch.y < Gdx.graphics.getHeight() / 2) directionY = -1;
		if(touch.x < Gdx.graphics.getHeight() / 2) directionX = -1;
		if(touch.x < Gdx.graphics.getHeight() / 2) directionX = -1;*/
		addHuman(touch.x, touch.y);
		//Gdx.app.log("TOUCH: ",  "" + pepsee.toIsometricGridPoint(new Vector3(touch)));
		//camera.unproject(touch);
		//shooter.getIsoTouchPoint(touch);
		//world.giso.yaz = true;
		//world.giso.logNow(touch);
	}
	
	public void selectNow(Vector3 touch){
		pepsee.yaz = true;
		pepsee.selectNow(touch);
	}
	
	@Override
	public void update(float delta) {
		//camera.update();
		pepsee.update(delta);
	}

	int typeH = 10;
	public boolean walking = false;
	@Override
	public void draw(float delta) {
		Gdx.gl.glClearColor(216/255f, 255/255f, 167/255f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		typeH = 10;
		//fps.log();

		pepsee.draw(delta);

		if(humans.size > 0){
			Human active = humans.get(0);
			if(tweenDone){
				if(Math.abs(active.getPosition().x - pepsee.camera.position.x) > 200 || Math.abs(active.getPosition().y - pepsee.camera.position.y) > 100){
					//tweenTo("UP", active.getPosition().x, active.getPosition().y, active.getPosition().z, 1f);
				}
			}else{
				//pepsee.camera.position.x = getTweenShadow().values.x;
				//pepsee.camera.position.y = getTweenShadow().values.y;
			}
			pepsee.camera.position.x = active.getPosition().x;
			pepsee.camera.position.y = active.getPosition().y;
			log("Human[0] real position: " + getVector3Info(active.getPosition()));
			Vector3 screenPoint = getLikeTouchPoint(active.getPosition());

			//Vector3 isoPos = new Vector3(active.getPosition());
			//active.setActiveCellCoordinateX((int) likeTouchPoint.x);
			//active.setActiveCellCoordinateY((int) likeTouchPoint.y);
			log("Human[0] screen position: " + getVector3Info(screenPoint));
			
			Vector3 pos = pepsee.toIsometricGridPoint(new Vector3(active.getPosition()));
			active.setPositionIsometric(pos);
			log("Human[0] isomet position: " + getVector3Info(pos));
			
			log("Human[0] Width Height: " + active.getWidth() + ", " + active.getHeight());
			log("FPS: " + Gdx.graphics.getFramesPerSecond() + "");
			/*
			//Cell cell = world.getCellFromScreenPoint(isoTile ,"foreground");
			//typer.render("Human[0] cell property: " + cell.getTile().getProperties().get("h"), 20, 80, 0.3f);
			log("Human[0] cell coord: " + active.getActiveCellCoordinateX() + ", " + active.getActiveCellCoordinateY());
			*/
		}
		
		humans.sort(pepsee.isometricObjectComparator);
		for (Human h : humans) {
			//Vector3 likeTouchPoint = getLikeTouchPoint(h.getPosition());
			Vector3 newPos = null;
			
			//typer.render("Human[0] screen position: " + getVector3Info(h.getPosition()), 20, typeH += 25, 0.3f);
			//selectNow(likeTouchPoint);
			//Vector3 isoPos = new Vector3(active.getPosition());
			//h.setActiveCellCoordinateX((int) likeTouchPoint.x);
			//h.setActiveCellCoordinateY((int) likeTouchPoint.y);
			//h.setPositionLikeTouchPoint(likeTouchPoint);
			//typer.render("Human[0] isomet position: " + getVector3Info(likeTouchPoint), 20, typeH += 25, 0.3f);
			
			if (vertical && walking) {
				if (directionY == 1)
					h.updateAnimationSet(Human.WALK_UP);
				if (directionY == -1)
					h.updateAnimationSet(Human.WALK_DOWN);
				newPos = new Vector3(h.getPosition().x + 0f * directionX, h.getPosition().y + 2.4f * directionY, h.getPosition().z);
				//newPos = getLikeTouchPoint(newPos);
				if(pepsee.positionAvailableWithHeight(h, new Vector3(newPos)))
					h.setPositionShadow(newPos);
			}
			if (!vertical && walking) {
				if (directionX == 1)
					h.updateAnimationSet(Human.WALK_RIGHT);
				if (directionX == -1)
					h.updateAnimationSet(Human.WALK_LEFT);
				newPos = new Vector3(h.getPosition().x + 2.4f * directionX , h.getPosition().y, h.getPosition().z);
				//newPos = getLikeTouchPoint(newPos);
				if(pepsee.positionAvailableWithHeight(h, new Vector3(newPos)))
					h.setPositionShadow(newPos);
			}
			//h.animation.setAnimationSet(h.animation.activeAnimationSetId);
			//h.getSirrusAnimation().draw(delta);
			if(walking){
				//Gdx.app.log("normal", h.getPosition() + "");
			}
			//world.giso.selectNow(isoTouchPoint);
			typeH = 0;
		}
		
		
		
		
		
	}
	
	private Vector3 getLikeTouchPoint(Vector3 pos){
		Vector3 poz = pepsee.camera.project(new Vector3(pos));		
		return new Vector3(poz.x, Gdx.graphics.getHeight() - poz.y, poz.z);
	}
	private String getVector3Info(Vector3 v){
		return "x:" + (int)v.x + ",  y:" +  (int)v.y + ", z:" + (int)v.z;
	}
	private String getVector2Info(Vector2 v){
		return "x:" + (int)v.x + ",  y:" +  (int)v.y + "";
	}
	
	
	private void log(String text){
		typer.render(text, 20, typeH += 15, 1f);
	}
	
	public Vector3 getIsoTouchPoint(Vector3 touch){
		Vector3 isoGridPoint = PepseeAbstract.toIsometricGridPoint(
				pepsee.camera.unproject(
						new Vector3(touch)), pepsee.mapTileWidth * pepsee.scale, pepsee.mapTileHeight * pepsee.scale);
		//Vector3 isoRealPoint = new Vector3(isoGridPoint.x * 64, isoGridPoint.y * 32, 0);
		float percentAB = isoGridPoint.y * 100 / pepsee.mapHeight;
		float percentAD = isoGridPoint.x * 100 / pepsee.mapWidth;
		float x = Gdx.graphics.getWidth() * percentAB / 100;
		float y = Gdx.graphics.getHeight() * percentAD / 100;
		if(percentAB < 50){
			//x *= -1;
		}
		if(percentAD > 50){
			//y *= -1;
		}
		return new Vector3(x, y, 0);
	}
	
	@Override
	public void resize(int width, int height) {
		zero = new Vector2(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		pepsee.resize(width, height);
	}



	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setGame(SirrusGame game) {
		this.game = game;
	}

}
