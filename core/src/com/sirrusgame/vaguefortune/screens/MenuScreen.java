package com.sirrusgame.vaguefortune.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.sirrusgame.sirrusgamebase.SirrusGame;
import com.sirrusgame.sirrusgamebase.SirrusScreen;
import com.sirrusgame.inputprocessors.MenuInputProcessor;

public class MenuScreen extends SirrusScreen{

	OrthographicCamera cam;
	BitmapFont font12;
	SpriteBatch batch;
	Texture bgImage;
	int centerX, centerY;
	Sprite sprite;
	
	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		cam.update();
		
		
		batch.setProjectionMatrix(cam.combined);
		batch.begin();
		sprite.draw(batch);
		batch.end();
		
	}

	@Override
	public void create() {
		bgImage = new Texture(Gdx.files.internal("graphs/menu.png"));
		//bgImage = new Texture(this.getGame().getSirrusAssets().getManager().get(""));
		bgImage.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		TextureRegion region = new TextureRegion(bgImage, 0, 0, 1024, 578);
		sprite = new Sprite(region);
		sprite.setSize(1f, 1f * sprite.getHeight() / sprite.getWidth());
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2);

		centerX = Gdx.graphics.getWidth() / 2;
        centerY = Gdx.graphics.getHeight() / 2;
        
        float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		cam = new OrthographicCamera(1, h/w);

		batch = new SpriteBatch();	
		this.getInputMultiplexer().addProcessor(new MenuInputProcessor(this));
		
		
		
	}

	@Override
	public void setGame(SirrusGame game) {
		this.game = game;
	}

}
