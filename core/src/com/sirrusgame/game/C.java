package com.sirrusgame.game;

public interface C {
	public static final int SCREEN_SPLASH = 0;
	public static final int SCREEN_MENU		= 1;
	public static final int SCREEN_LOADING = 2;
	public static final int SCREEN_GAME = 3;

	public static final int NORTH = 0;
	public static final int SOUTH = 1;
	public static final int WEST = 2;
	public static final int EAST = 3;
	
	public static final String ASSET_TEXTURE_CHARACTERS = "graphs/characters.png";
	public static final String ASSET_FONT_UNISPACE = "fonts/unispace.fnt";
}
