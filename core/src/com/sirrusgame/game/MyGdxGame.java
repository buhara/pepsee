package com.sirrusgame.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sirrusgame.sirrusgamebase.SirrusGame;
import com.sirrusgame.sirrusgamebase.SirrusLoadScreen;
import com.sirrusgame.sirrusgamebase.SirrusSplashScreen;
import com.sirrusgame.sirrusgamebase.objects.SirrusScreenDescription;
import com.sirrusgame.vaguefortune.screens.GameScreen;
import com.sirrusgame.vaguefortune.screens.MenuScreen;

public class MyGdxGame  extends SirrusGame  {
	
	public MyGdxGame() {
	}

	@Override
	public void create() {
		//SirrusScreen sc = (SirrusScreen) getDescriptionFromId(C.SCREEN_SPLASH).getScreen();
		this.setSirrusScreen(C.SCREEN_SPLASH);
	}

	@Override
	public void setSirrusScreens() {
		addSirrusScreenDescriptions(
				new SirrusScreenDescription(SirrusSplashScreen.class)
					.addAssets("graphs/splash.png")
					.setSplashScreen(true)
					.setId(C.SCREEN_SPLASH)
					.setNextScreenId(C.SCREEN_GAME)
				);
		addSirrusScreenDescriptions(
				new SirrusScreenDescription(MenuScreen.class)
					.setId(C.SCREEN_MENU)
					.setNextScreenId(C.SCREEN_LOADING)
				);
		addSirrusScreenDescriptions(
				new SirrusScreenDescription(SirrusLoadScreen.class)
					.setId(C.SCREEN_LOADING)
					.addAssets(C.ASSET_TEXTURE_CHARACTERS, Texture.class)
					.addAssets(C.ASSET_FONT_UNISPACE, BitmapFont.class)
					.setNextScreenId(C.SCREEN_GAME)
				);
		addSirrusScreenDescriptions(
				new SirrusScreenDescription(GameScreen.class)
					.setScreen(GameScreen.class)
					.addAssets(C.ASSET_TEXTURE_CHARACTERS, Texture.class)
					.setId(C.SCREEN_GAME)
					.setNextScreenId(C.SCREEN_MENU)
				);
	}
}
